
//1
db.fruits.aggregate([
        {$match:{onSale:true}},
        {$count: "FruitsOnSale"}
      ]);


//2
db.fruits.aggregate([
        {$match:{stock:{$gt: 20}}},
        {$count: "EnoughStock"}
      ]);


//3
db.fruits.aggregate([
        {$match:{onSale:true}},
        {$group: {_id:"$supplier_id", 
        avg_price: {$avg: "$price"}}}
      ]);


//4
db.fruits.aggregate([
        {$group: {_id:"$supplier_id", 
        max_price: {$max: "$price"}}}
      ]);

//5
db.fruits.aggregate([
        {$group: {_id:"$supplier_id", 
        min_price: {$min: "$price"}}}
      ]);